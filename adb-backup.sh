#!/bin/bash -e
# Copyright (c) 2022 "Jason Schmidt" <zannalov@gmail.com>
# This program is free software. It comes without any warranty, to the extent
# permitted by applicable law. You can redistribute it and/or modify it under
# the terms of the Do What The Fuck You Want To Public License, Version 2, as
# published by Sam Hocevar. See http://www.wtfpl.net/ for more details.

# Check that BASH version is high enough
# https://unix.stackexchange.com/a/285928
if ( printf '%s\n3.2.57\n' "$BASH_VERSION" | sort -VC ); then
  echo "Bash version must be 3.2.57 or greater, current verison is: $BASH_VERSION" >&2
  exit 1
fi

# realpath polyfill
if ( ! hash realpath &>/dev/null ); then
  function realpath () {
    local path="$1"
    perl -MCwd -le 'print Cwd::abs_path shift' "$path"
  }
fi

function fileSafeDate () {
  date -u +%Y-%m-%d-%H%M%SZ
}

# This script
thisScript="$0"
thisScriptName="$(basename "$thisScript")"
scriptStarted="$(fileSafeDate)"

# Deferred logging (things that should be logged but before log file has been
# created
inMemoryLog=""
tempFiles=( )
backgrounded=( )

# Global options
outputDirname=DEFAULT
forceAppendDate=DEFAULT
adbPath=DEFAULT
adbfsPath=DEFAULT
javaPath=DEFAULT
abeAllPath=DEFAULT
abeIgnoreNonzeroExit=DEFAULT
debug=DEFAULT
pairAddress=DEFAULT
pairCode=DEFAULT
dryRun=DEFAULT
toggleStayAwake=DEFAULT
disableAdbOnComplete=DEFAULT
logcat=DEFAULT
initialTab=DEFAULT
desktopBackupPasswordSet=DEFAULT

# ab mode options
abEncrypted=1
abPassword="" # Note: Empty string means use deviceId, set encrypted=0 for unencrypted
abAppClass="all"
abPackage="" # Note: Only applies of appclass=package
abIncludeApk=1
abIncludeObb=1
abSkipAfterFirstAppSuccess=1
abWaitingForBackupConfirmationScreenTimeout=10

# sdcard-excludes options
sdcardExcludes=""

# Log of global options for runtime script
logGlobalOptions=( )
logModesAndOperations=( )

# Modes and operations arguments to be executed (excludes globals)
modesAndOperations=( )
allPackageListFiles=( )

function usage() {
more <<USAGE
Usage: $thisScriptName [global options] <[mode options] operation>...
  DEFAULT: $thisScriptName dcim ab sdcard installers

This script takes in a list of optional global options, and a set of operations
to perform (with optional mode modifiers for those operations).

Each global option may only appear once in the list of arguments.

Each operation will be performed in the order specified, with the preceding
mode options applied to the operation.

Global Options

  -o dirname | --output=dirname | --output dirname
    DEFAULT: <deviceId>-<datetime>
    Override/resume output dir

  --force-append-date
    Add a filename safe datestamp to the end of the output directory name.
    WARNING: Do not use when resuming a previous job.

  --debug
    Print everything to stdout/stderr instead of logging

  --dry-run
    Print everything that would be done, but don't actually execute

  --device=id | --device id
    DEFAULT: Uses attached device if only one
    Requires given device id
    May be an IP address (defaults to port 5555) or IP:PORT

  --pair ip:port code
    Must be used in conjunction with --device=ip:port
    Will automatically run "adb pair" followed by "adb connect"

  --adb=path | --adb path
    DEFAULT: Searches path for "adb" command
    Specifies location of adb

  --java=path | --java path
    DEFAULT: Searches path for "java" command
    Specifies location of java for running abe-all.jar

  --abe-all-jar=path | --abe-jar path
    DEFAULT: Searches path for "abe-all.jar"
    Specifies location of abe-all.jar

  --ignore-abe-all-jar-error
    DEFAULT: Fails on non-zero exit code from abe-all.jar
    If set, ignores non-zero exit code from abe-all.jar

  --no-stay-awake
    If this parameter is passed, the device setting for staying awake while
    plugged in will not be toggled (neither enabled nor disabled).

  --no-disable-adb
    If this parameter is passed, usb debugging (adb) will remain enabled on the
    device after the script exits. Without this parameter, the last thing the
    script does will be to attempt to disable usb debugging on the device (for
    security).

  --no-logcat
    By default the script performs a logcat before and after backup operations
    are completed. This option suppresses that behavior.

  --no-initial-tab
    Older phones may not require tabbing into the password field. This disables
    the initial keyboard tab sent to get into the password field. Try this
    option if you see the password entry field come up on screen and no
    password gets entered.

  --desktop-backup-password-is-set
    If the "Desktop backup password" is set, then we assume --password contains
    that password and use it for all .ab backups. However this requires an
    extra step to get to the "Back up my data" button.

Operations

  pull <A> <B>
    Backup remote folder A to local folder B
    Note: B is relative to output dirname (see -o)

  dcim
    Alias for "pull /storage/emulated/0/DCIM ."

    Note: adb pull always creates a local directory when pulling a directory,
    so this really stores the contents of DCIM in ./DCIM

  sdcard
    Alias for "pull /storage/emulated/0 ."

    Note: adb pull always creates a local directory when pulling a directory,
    so this really stores the contents of sdcard in ./0

  cleanup
    Deletes ./sdcard/Android/obb and ./sdcard/DCIM if they are duplicates of
    ./app/*.obb and ./DCIM

  sdcard-excludes
    Similar function to "pull /storage/emulated/0 ." except that it uses tar natively
    on the device to provide greater flexibility (the ability to exclude
    specific directories instead of transferring files and then needing to run
    cleanup).

  rsync <A> <B>
    Uses adbfs (must be present along with fuse) to run a recursive rsync
    preserving links and times (but not usernames or permissions). When
    resuming a halted execution (or if destination directory is prepopulated
    with previous data) this will efficiently skip unchanged files. Uses the
    same options as sdcard-excludes.
    Note: B is relative to output dirname (see -o)

  installers
    Attempt to pull application APKs and OBBs (raw files)

  ab
    Attempt to backup apps using adb backup

  recommended
    Alias for
      dcim                                            # Start by getting pictures and videos as highest priority
      --appclass=3rdparty --ab-dao ab ab --ab-d ab    # Next try twice to get 3rd part apps with apk and obb, then once for any failed 3rd party apps with only data
      sdcard                                          # Then get the contents of the sdcard in case there are other files of interest
      --appclass=system --ab-dao ab --ab-d ab         # Next try once for system apps with apk and obb, and once for any failed ones getting only the data
      installers                                      # Then explicitly back up the apk and obb files of all apps (NOT .ab files)
      cleanup                                         # Having now fetched dcim and obb (via installers), clean these up
      --appclass=disabled --ab-dao ab                 # Finally as lowest priority, try to back up disabled apps

Mode options for "ab" (these will apply to all following executions of "ab")

  Encryption

    --unencrypted | --encrypted
      DEFAULT: --encrypted
      Don't or do encrypt the ab file.
      Uses device ID as password unless one is specified.

    --password-from-device-id | --password=<password>
      DEFAULT: --password-from-device-id
      If --encrypted specified, use this password

  Package Selection

    --appclass=<all|3rdparty|system|disabled>
      DEFAULT: --appclass=all

      Iterate over all packages in the given class and make separate ab files
      for each during the next "ab"

    -p=<package> | --package=<package>
      Limit next "ab" to a single package

  Package Contents

    --ab-dao | --ab-da | --ab-d
      DEFAULT: --ab-dao
      "d" represents app data, "a" represents app apk, and "o" represents app obb

  Retry Logic

    --skip-success | --retry-success
      DEFAULT: --skip-success
      If a given app succeeded in a previous attempt (for any mode) skip it, or retry it anyway
      For example: -p package --skip-success --ab-dao ab --ab-da ab --ab-d ab
        If the first attempt of "dao" mode succeeded, a single file would be
        created: package.apk.obb.ab
        and the other two attempts would be SKIPPED.
      For example: -p package --retry-success --ab-dao ab --ab-da ab --ab-d ab
        If each of the attempts succeeded, three files would be created:
        package.apk.obb.ab
        package.apk.no-obb.ab
        package.no-apk.no-obb.ab

  Timeout

    --ab-backupconfirm-timeout=<seconds>
      DEFAULT: 10
      How long to wait for the adb backup confirmation screen before timing out
      and attempting to recover.

sdcard-excludes and rsync options

  --clear-excludes = empty the list
    Empties the list of excludes

  --exclude=x | --exclude x
    Add x to the excludes list
    NOTE: Excludes are already relative to the "A" path of the command.
    NOTE: Excludes do NOT need to begin with a slash.

  --reset-excludes
    Equivalent to --clear-excludes --exclude=DCIM --exclude=Android/obb

USAGE
}

function instructHowToGetHelpAndExit () {
  echo "Usage: $thisScriptName <-h|--help>" >&2
  exit 1
}

function checkGlobalPathIsFile () {
  local filePath="$1"
  local option="$2"

  if [[ ! -f "$filePath" ]]; then
    echo "Invalid path. $option must be a file: $filePath" >&2
    instructHowToGetHelpAndExit
  fi
}

function checkGlobalPathIsExecutableFile () {
  local filePath="$1"
  local option="$2"

  checkGlobalPathIsFile "$filePath" "$option"

  if [[ ! -x "$filePath" ]]; then
    echo "Invalid path. $option must be an executable file: $filePath" >&2
    instructHowToGetHelpAndExit
  fi
}

function setGlobalOption () {
  local globalOptionName="$1"
  local newValue="$2"
  local description="$3"
  shift 3
  local setRunScriptGlobalOptions=( "$@" )

  # If the global setting already has a non-default value, then set it to the
  # provided value
  if [[ "${!globalOptionName}" != "DEFAULT" ]]; then
    echo "ERROR: More than one $description global option provided. Please see usage." >&2
    instructHowToGetHelpAndExit
  fi

  # Set an environment variable to the output of printf. Using %s to prevent
  # interpretation of value by printf
  printf -v "$globalOptionName" "%s" "$newValue"

  if [[ ${#setRunScriptGlobalOptions} -gt 0 ]]; then
    logGlobalOptions+=( "${setRunScriptGlobalOptions[@]}" )
  fi
}

function parseScriptArguments () {
  # Parse all script arguments
  local abOperationChanges=( )
  local sdcardExcludeOptions=( )
  while [[ $# -gt 0 ]]; do
    case "$1" in
      -h|--help|/?|/h)
        usage
        exit 0
        ;;

      # Global options
      --output=*)
        setGlobalOption outputDirname "${1#--output=}" "output dirname"
        shift 1
        ;;
      -o|--output)
        setGlobalOption outputDirname "$2" "output dirname"
        shift 2
        ;;
      --pair)
        setGlobalOption pairAddress "$2" "pairing address"
        setGlobalOption pairCode "$3" "pairing code"
        shift 3
        ;;
      --force-append-date)
        setGlobalOption forceAppendDate true "append date to output dirname"
        shift 1
        ;;
      --debug)
        setGlobalOption debug 1 "debug" "$1"
        shift 1
        ;;
      --dry-run)
        setGlobalOption dryRun 1 "dry run" "$1"
        shift 1
        ;;
      --no-stay-awake)
        setGlobalOption toggleStayAwake 0 "stay awake while charging" "$1"
        shift 1
        ;;
      --no-disable-adb)
        setGlobalOption disableAdbOnComplete 0 "disable adb when complete" "$1"
        shift 1
        ;;
      --no-logcat)
        setGlobalOption logcat 0 "logcat" "$1"
        shift 1
        ;;
      --no-initial-tab)
        setGlobalOption initialTab 0 "initialTab" "$1"
        shift 1
        ;;
      --desktop-backup-password-is-set)
        setGlobalOption desktopBackupPasswordSet 1 "desktopBackupPasswordSet" "$1"
        shift 1
        ;;
      --device=*)
        echo -n "${1#--device=}" > deviceId.txt
        shift 1
        ;;
      --device)
        echo -n "${2}" > deviceId.txt
        shift 2
        ;;
      --adb=*)
        setGlobalOption adbPath "$(realpath "${1#--adb=}")" "adb path" "$1"
        checkGlobalPathIsExecutableFile "$adbPath" "--adb"
        shift 1
        ;;
      --adb)
        setGlobalOption adbPath "$(realpath "$2")" "adb path" "$1" "$2"
        checkGlobalPathIsExecutableFile "$adbPath" "--adb"
        shift 2
        ;;
      --adbfs=*)
        setGlobalOption adbfsPath "$(realpath "${1#--adbfs=}")" "adbfs path" "$1"
        checkGlobalPathIsExecutableFile "$adbfsPath" "--adbfs"
        shift 1
        ;;
      --adbfs)
        setGlobalOption adbfsPath "$(realpath "$2")" "adbfs path" "$1" "$2"
        checkGlobalPathIsExecutableFile "$adbfsPath" "--adbfs"
        shift 2
        ;;
      --java=*)
        setGlobalOption javaPath "$(realpath "${1#--java=}")" "java path" "$1"
        checkGlobalPathIsExecutableFile "$javaPath" "--java"
        shift 1
        ;;
      --java)
        setGlobalOption javaPath "$(realpath "$2")" "java path" "$1" "$2"
        checkGlobalPathIsExecutableFile "$javaPath" "--java"
        shift 2
        ;;
      --abe-all-jar=*)
        setGlobalOption abeAllPath "${1#--abe-all-jar=}" "abe-all-jar path"
        checkGlobalPathIsFile "$abeAllPath" "--abe-all-jar"
        shift 1
        ;;
      --abe-all-jar)
        setGlobalOption abeAllPath "$2" "abe-all-jar path"
        checkGlobalPathIsFile "$abeAllPath" "--abe-all-jar"
        shift 2
        ;;
      --ignore-abe-all-jar-error)
        setGlobalOption abeIgnoreNonzeroExit 1 "ignore-abe-all-jar-error" "$1"
        shift 1
        ;;

      # Operations
      pull)
        modesAndOperations+=( "${abOperationChanges[@]}" pull "$2" "$3" )
        logModesAndOperations+=( "${abOperationChanges[@]}" pull "$2" "$3" )
        abOperationChanges=( )
        shift 3
        ;;
      dcim|sdcard|cleanup|installers|ab)
        modesAndOperations+=( "${abOperationChanges[@]}" "$1" )
        logModesAndOperations+=( "${abOperationChanges[@]}" "$1" )
        abOperationChanges=( )
        shift 1
        ;;
      recommended)
        modesAndOperations+=( "${abOperationChanges[@]}"
          dcim
          --appclass=3rdparty --ab-dao ab ab --ab-d ab
          sdcard
          --appclass=system --ab-dao ab --ab-d ab
          installers
          cleanup
          --appclass=disabled --ab-dao ab
        )
        logModesAndOperations+=( "${abOperationChanges[@]}" "$1" )
        abOperationChanges=( )
        shift 1
        ;;

      sdcard-excludes)
        modesAndOperations+=( "${sdcardExcludeOptions[@]}" "$1" )
        logModesAndOperations+=( "${sdcardExcludeOptions[@]}" "$1" )
        sdcardExcludeOptions=( )
        shift 1
        ;;

      rsync)
        modesAndOperations+=( "${sdcardExcludeOptions[@]}" "$1" "$2" "$3" )
        logModesAndOperations+=( "${sdcardExcludeOptions[@]}" "$1" "$2" "$3" )
        sdcardExcludeOptions=( )
        shift 3
        ;;

      # AB modes
      --unencrypted|--encrypted|--password-from-device-id|--password=*|--appclass=*|-p=*|--package=*|--ab-dao|--ab-da|--ab-d|--skip-success|--retry-success|--ab-backupconfirm-timeout=*)
        abOperationChanges+=( "$1" )
        shift 1
        ;;
      --password|--appclass|-p|--package|--ab-backupconfirm-timeout)
        abOperationChanges+=( "$1" "$2" )
        shift 2
        ;;

      # sdcard-excludes options
      --reset-excludes|--clear-excludes|--exclude=*)
        sdcardExcludeOptions+=( "$1" )
        shift 1
        ;;
      --exclude)
        sdcardExcludeOptions+=( "$1" "$2" )
        shift 2
        ;;

      # Catch unexpected arguments
      *)
        echo "Unexpected argument: $1" >&2
        instructHowToGetHelpAndExit
        ;;
    esac
  done
  if [[ "${#abOperationChanges}" -gt 0 ]]; then
    echo "The following ab mode changes were set, but the ab operation was never executed afterward: ${abOperationChanges[@]}" >&2
    instructHowToGetHelpAndExit
  fi
  if [[ "${#sdcardExcludeOptions}" -gt 0 ]]; then
    echo "The following sdcard-excludes options were set, but no sdcard-excludes or rsync operation was found afterward: ${sdcardExcludeOptions[@]}" >&2
    instructHowToGetHelpAndExit
  fi
}

function assignGlobalDefault () {
  local globalOptionName="$1"
  local defaultValue="$2"

  if [[ "${!globalOptionName}" == "DEFAULT" ]]; then
    if [[ "$defaultValue" != "" ]]; then
      printf -v "$globalOptionName" "%s" "$defaultValue"
    else
      unset $globalOptionName
    fi
  fi
}

function assignGlobalDefaults () {
  assignGlobalDefault adbPath ""
  assignGlobalDefault adbfsPath ""
  assignGlobalDefault javaPath ""
  assignGlobalDefault abeAllPath ""
  assignGlobalDefault abeIgnoreNonzeroExit "0"
  assignGlobalDefault debug "0"
  assignGlobalDefault dryRun "0"
  assignGlobalDefault pairAddress ""
  assignGlobalDefault pairCode ""
  assignGlobalDefault toggleStayAwake 1
  assignGlobalDefault disableAdbOnComplete 1
  assignGlobalDefault logcat 1
  assignGlobalDefault initialTab 1
  assignGlobalDefault desktopBackupPasswordSet 0
}

function printBashScriptForThisRun () {
  printf '#!/bin/bash -e\n'

  # Export a comment line containing the raw originally typed command, which
  # should be the arguments to this function
  printf '# '
  while [[ $# -gt 0 ]]; do
    printf '%q ' "$1"
    shift 1
  done
  printf '\n'

  # Hard-coded values based on how this script works. abe-all.jar is always
  # copied into this directory, as is this script. Resuming via this run script
  # should be based on the previously provided or detected device id (don't
  # allow it to switch devices on resume/rerun). Output to the same directory
  # as this script.
  printf 'exec ../%s \\\n' "$thisScriptName"
  printf '  --abe-all-jar=./abe-all.jar \\\n'
  printf '  --device=%q \\\n' "$(cat deviceId.txt)"
  printf '  --output=. \\\n'

  # Write all raw global options, then all modes and operations
  for v in "${logGlobalOptions[@]}" "${logModesAndOperations[@]}" ; do
    printf '  %q \\\n' "$v"
  done

  # End the argument lines for the script
  printf '  # End of script options\n'
}

function createBashScriptForThisRun () {
  if [[ $dryRun == 1 ]]; then
    echo "Would write the following to ./runs/$scriptStarted.sh"
    printBashScriptForThisRun "$@" | sed 's/^/  /g'
  else
    printBashScriptForThisRun "$@" > ./runs/$scriptStarted.sh
    chmod u+x ./runs/$scriptStarted.sh
  fi
}

function adbPull() {
  local remoteFolder="$1"
  local localFolder="$2"
  local exitCode
  local adbPullCommand

  if [[ "$dryRun" == 1 ]]; then
    echo "Would attempt to pull $remoteFolder from device into $outputDirname/$localFolder"
    return
  fi

  # Ensure cable didn't come loose
  waitForDevice "$(cat deviceId.txt)" --silent

  echo "Starting adb pull $remoteFolder $localFolder"

  adbPullCommand=( adb pull -a "$remoteFolder" "$localFolder" )

  # Perform pull. tee output into log file (append) while filtering to
  # displayed output. Take the beginning of the line "[nnn%] " and suffix the
  # folder. Make this a running output by prepending with a carriage return and
  # strippig newlines. This should provide a running percentage of the overall
  # adb pull command in progress, while preserving full logging. Use '#' for
  # the delimeter in sed because '/' could exist in a path, but '#' should be a
  # non-allowed filename character, and thus shouldn't exist in $localFolder.
  # tr needs to be the last command so that sed has whole lines upon which to
  # operate.
  if [[ "$debug" == 1 ]]; then
    # debugging, so no need to parse output to show a progress meter
    "${adbPullCommand[@]}"
    exitCode=$?
  else
    # NOTE: Explicitly going directly to native stdout because this should
    # result in just the percentage being shown on the command line as it
    # changes, and the raw output should end up going to the log file
    "${adbPullCommand[@]}" | tee >( grep -E -e '^\[[0-9 ]{3}%\] ' | sed "s#^\(.\{6\}\).*#"$'\r'"\1 $localFolder"$'\r'"#" | stdbuf -o0 tr -d $'\n' >&4 ) >&3
    exitCode=${PIPESTATUS[0]}
    echo >&4 # Output ended, make sure we move to the next line
  fi

  if [[ $exitCode != 0 ]]; then
    # Fail soft on this one since a file transfer error will end the entire backup, but make it stand out
    echo "ADB PULL EXIT CODE NOT ZERO: $?" >&2
  fi

  echo "Completed adb pull $remoteFolder $localFolder"
}

function getPackageList () {
  local file="$1"
  shift 1

  allPackageListFiles+=( "$file" )

  # Get the list of installed packages, stripping off the "package:" prefix
  # which pm adds to each line
  adb shell pm list packages "$@" 2>&1 | tr -d '\r' | sed 's/^package://' > "$file"

  echo "$file" >&3
  cat "$file" | sed 's/^/  /' >&3
}

function getPackageLists () {
  # First get the list of all 3rd party packages that are enabled (highest priority to back up)
  getPackageList packages.enabled.3rdparty.txt -e -3

  # Next get the list of all system packages that are enabled (second highest priority to back up)
  getPackageList packages.enabled.system.txt -e -s

  # Finally get the list of all disabled packages (low priority to back up)
  getPackageList packages.disabled.txt -d
}

function getPackageListsIfNotYetFetched () {
  if [[ "${#allPackageListFiles}" == 0 ]]; then
    getPackageLists
  fi
}

function verifyBackup () {
  local backupFile="$1"
  local password="$2"
  local unpackExitCode
  local tarExitCode

  echo "Attempting to verify backup $backupFile" >&3
  if ! test -e "$backupFile"; then
    echo "Backup failed, unable to find $backupFile" >&3
    return -1
  fi

  # Ensure no leftovers from previous run
  rm -vf .temp.tar .temp.tar.tf >&3

  # Log details about file
  ls -l "$backupFile" >&3

  # If .ab file is empty
  if ( ! test -s "$backupFile" ); then
    echo "Backup file $backupFile has zero length, invalid backup" >&3
    return -1
  fi

  # Attempt to unpack backup
  java -jar abe-all.jar unpack "$backupFile" .temp.tar "$password" >&3 2>&3 ; unpackExitCode=$?
  if [[ $unpackExitCode != 0 ]] && [[ $abeIgnoreNonzeroExit == 0 ]]; then
    rm -vf .temp.tar >&3
    echo "Extract command failed $unpackExitCode" >&3
    return -1
  fi
  if ( ! test -e .temp.tar ); then
    echo "Extract command failed to create tar file" >&3
    return -1
  fi

  # Attempt to read unpacked backup
  tar tf .temp.tar 2>/dev/null | grep -v '/_manifest$' >.temp.tar.tf ; tarExitCode=$?
  if [[ $tarExitCode != 0 ]] || ( ! test -e .temp.tar.tf ); then
    rm -vf .temp.tar .temp.tar.tf >&3
    echo "Unable to extract file list from backup tar $tarExitCode" >&3
    return -1
  fi
  echo "Contents of .temp.tar.tf (excluding manifest)" >&3
  sed 's/^/  /' .temp.tar.tf >&3

  # Get linecount
  filecount="$(cat .temp.tar.tf 2>/dev/null | wc -l)"
  echo "filecount=$filecount" >&3

  # Clean up temporary unpacked file
  rm -vf .temp.tar .temp.tar.tf >&3

  # If everything ran successfully, exit successfully
  if test $filecount -gt 0; then
    echo "Backup verified" >&3
    return 0
  fi

  # If anything fell through, fail
  echo "Backup failed, backup was empty" >&3
  return -1
}

function checkForBackupScreenVisible () {
  adb shell dumpsys window 2>/dev/null | egrep '(mCurrentFocus|mFocusedWindow)=.*BackupRestoreConfirmation' &>/dev/null
  return $?
}

function keepAwake () {
  while ( test -e .keep-awake.txt ); do
    # Note: Runs in a subshell, so won't pick up updates to ANDROID_SERIAL, so
    # make it refresh by rereading the file. Don't output anything to the user.
    ANDROID_SERIAL="$(cat deviceId.txt)" adb shell input keyevent mouse >&3 2>&3
    sleep 1
  done
}

function startKeepAwake () {
  stopKeepAwake
  touch .keep-awake.txt
  keepAwake &
  echo -n $! > .keep-awake.txt
}

function stopKeepAwake () {
  local pid

  # If it wasn't already running, skip the stop command
  if ( ! test -e .keep-awake.txt ); then
    return
  fi

  # Get the contents of the file and delete it, which should also cause the
  # inner loop of keepAwake to exit
  pid="$(cat .keep-awake.txt)"
  rm -f .keep-awake.txt

  # If file was empty, then it was starting up and we won't have a PID to wait
  # on, so exit early
  if ( ! test -n "$pid" ); then
    return
  fi

  # Just in case removing the file isn't enoough on its own, kill PID, but
  # don't error if this command fails
  kill $pid >/dev/null 2>&1 || true

  # Wait doesn't work on commands started from a differnt subshell, so wait for
  # /proc/pid to disappear instead
  while ( kill -0 $pid >/dev/null 2>&1 ); do
    sleep 1
  done
}

# This command takes care of running one "adb backup" command provided to it
function runBackupCommand () {
  # Ensure cable didn't come loose
  waitForDevice "$(cat deviceId.txt)" --silent

  # Make sure the device has aborted the backup confirmation (so that we cannot
  # accidentally enter the same password twice in subsequent runs
  adb shell pm clear com.android.backupconfirm &>/dev/null

  # Make backup screen appear with provided adb command
  echo "$@" >&3
  "$@" >&3 2>&3 &

  # Wait for the backup confirmation window to appear
  echo "Waiting for backup confirmation screen..." >&3
  countdown=$abWaitingForBackupConfirmationScreenTimeout
  sleep 1
  while ( ! checkForBackupScreenVisible ) && ( test $countdown -gt 0 ); do
    let countdown--
    echo "Still waiting..." >&3
    sleep 1
  done
  if [[ $countdown == 0 ]]; then
    echo "Waited too long" >&3

    # Clean up server-side
    echo "Killing adb server..." >&3
    adb kill-server
    killall adb || echo "OK if no processes found" >&3
    rm -vf .in-progress.ab >&3

    connect
    waitForDevice "$(cat deviceId.txt)" # Performs its own output

    echo "Aborted command $@" >&3
    return 1
  fi

  local initialTabOptions=""
  local extraTab=""

  # If desktop backup password is set, then the password gets entered into the
  # confirmation field and we leave the password for this backup blank (causing
  # it to use the desktop password), but this means we need to tab through the
  # extra password field
  if [[ $desktopBackupPasswordSet == 1 ]]; then
    extraTab="KEYCODE_TAB"
  fi


  # Optionally input the password
  # Tap the backup button by navigating to it via key presses
  # http://stackoverflow.com/a/28969112
  if [[ "$abEncrypted" == 1 ]]; then
    if [[ $initialTab == 1 ]]; then
      initialTabOptions="input keyevent KEYCODE_TAB ;"
    fi

    echo "Entering device ID as password and simulating button selection" >&3
    adb shell $initialTabOptions input text "$abPassword" \; input keyevent KEYCODE_TAB KEYCODE_TAB $extraTab KEYCODE_ENTER
  else
    if [[ $initialTab == 1 ]]; then
      initialTabOptions="KEYCODE_TAB"
    fi

    echo "Simulating button selection" >&3
    # Note: Extra TAB because the "input text" command auto-focuses the input field, but we're skipping that step
    adb shell input keyevent $initialTabOptions KEYCODE_TAB KEYCODE_TAB $extraTab KEYCODE_ENTER
  fi

  # Wait for backup to complete
  echo "Waiting for backup to complete" >&3
  wait
}

function anyAttemptSucceeded () {
  local package="$1"
  test -e backups/"$package".apk.obb.ab -o -e backups/"$package".apk.no-obb.ab -o -e backups/"$package".no-apk.no-obb.ab
}

function abBackupPackage () {
  local package="$1"

  local apk="apk"
  local obb="obb"
  [[ $abIncludeApk == 0 ]] && apk='no-apk'
  [[ $abIncludeObb == 0 ]] && obb='no-obb'

  local backupFile="backups/$package.$apk.$obb.ab"

  # If any approach on any attempt for this app succeeded
  if [[ $abSkipAfterFirstAppSuccess == 1 ]]; then
    if ( anyAttemptSucceeded "$package" ); then
      echo "  Skipping $backupFile because one of the previous attempts succeeded for $package" >&3
      return
    fi
  else
    if [[ -e "$backupFile" ]]; then
      echo "  Skipping $backupFile because it already exists" >&3
      return
    fi
  fi

  # If the backup command succeeds
  if ( runBackupCommand adb backup -f .in-progress.ab -$apk -$obb -noshared "$package" ); then
    # And if the generated backup is valid
    if ( verifyBackup .in-progress.ab "$abPassword" ); then
      # Move it into place
      mv -v .in-progress.ab "$backupFile" >&3

      # Report the success
      echo "  Successfully backed up $backupFile"
    else
      echo "  FAILED $backupFile"
    fi
  else
    echo "  FAILED $backupFile"
  fi

  # In case of failure, clean up leftover .in-progress.ab
  rm -vf .in-progress.ab >&3
}

function abBackupBatch () {
  local packages
  local package
  local clioptions=""

  if [[ $dryRun == 1 ]]; then
    echo "Would run an ab backup with the following effective configuration:"
    echo "  abEncrypted=$abEncrypted"
    echo "  abPassword=$abPassword"
    echo "  abAppClass=$abAppClass"
    echo "  abPackage=$abPackage"
    echo "  abIncludeApk=$abIncludeApk"
    echo "  abIncludeObb=$abIncludeObb"
    echo "  abSkipAfterFirstAppSuccess=$abSkipAfterFirstAppSuccess"
    echo "  abWaitingForBackupConfirmationScreenTimeout=$abWaitingForBackupConfirmationScreenTimeout"
    return
  else
    if [[ "$abEncrypted" == 0 ]]; then
      clioptions="$clioptions --unencrypted"
    elif [[ "$abPassword" != "" && "$abPassword" != "$(cat deviceId.txt)" ]]; then
      clioptions="$clioptions --password=<...>"
    fi

    if [[ "$abPackage" != "" ]]; then
      clioptions="$clioptions --package=$abPackage"
    elif [[ "$abAppClass" != "all" ]]; then
      clioptions="$clioptions --appclass=$abAppClass"
    fi

    if [[ "$abIncludeApk" == 1 && "$abIncludeObb" == 1 ]]; then
      clioptions="$clioptions --ab-dao"
    elif [[ "$abIncludeApk" == 1 ]]; then
      clioptions="$clioptions --ab-da"
    else
      clioptions="$clioptions --ab-d"
    fi

    if [[ "$abSkipAfterFirstAppSuccess" != 1 ]]; then
      clioptions="$clioptions --retry-success"
    fi
    if [[ "$abWaitingForBackupConfirmationScreenTimeout" != 10 ]]; then
      clioptions="$clioptions --ab-backupconfirm-timeout=$abWaitingForBackupConfirmationScreenTimeout"
    fi

    echo "Starting adb backups to ab files with options: $clioptions"
  fi

  getPackageListsIfNotYetFetched

  mkdir -vp "backups" >&3
  mkdir -vp "backups/.attempts" >&3

  if [[ "$abEncrypted" == 1 && "$abPassword" == "" ]]; then
    abPassword="$(cat deviceId.txt)"
  fi

  case "$abAppClass" in
    package)
      packages="$abPackage"
      ;;
    all)
      packages="$(cat "${allPackageListFiles[@]}")"
      ;;
    3rdparty)
      packages="$(cat packages.enabled.3rdparty.txt)"
      ;;
    system)
      packages="$(cat packages.enabled.system.txt)"
      ;;
    disabled)
      packages="$(cat packages.disabled.txt)"
      ;;
  esac

  for package in $packages ; do
    ifPaused
    abBackupPackage "$package"
  done
}

function fetchPackageInstaller () {
  local package="$1"
  local apkPath

  # Ensure cable didn't come loose
  waitForDevice "$(cat deviceId.txt)" --silent

  echo "Attempting to get apks and obbs for $package" >&3
  if ( test -e apps/"$package".apk ); then
    echo "  Skipping $package because it has already been downloaded" >&3
  fi
  if ( ! test -e apps/"$package".apk ); then
    apkPath=$(adb shell pm path "$package" 2>&1 | tr -d '\r' | sed 's/^package://' | head -n 1)
    if ( test -n "$apkPath" ); then
      apkPath="$(dirname "$apkPath")"
      echo adb pull "$apkPath" apps/"$package".apk >&3
      adb pull "$apkPath" apps/"$package".apk >&3 && echo "  Fetched apk for $package" || echo "  Failed to get apk for $package" >&2
    else
      echo "  Unable to get path to apk for $package" >&2
    fi
  fi
  if ( test -e apps/"$package".apk ) && ( ! test -e apps/"$package".obb ); then
    echo "Trying to pull OBB files" >&3
    adb pull /sdcard/Android/obb/"$package" apps/"$package".obb >&3 && echo "    Fetched obb for $package" || echo "Pull obb files failed, may not have existed" >&3
  fi
}

function fetchPackageInstallers () {
  if [[ $dryRun == 1 ]]; then
    echo "Would attempt to get installers for all packages"
    return
  fi

  echo "Starting package installer download"

  getPackageListsIfNotYetFetched

  mkdir -vp "apps" >&3

  local package
  for package in $(cat "${allPackageListFiles[@]}"); do
    fetchPackageInstaller "$package"
  done

  echo "Done fetching installers. $(( $(ls -d apps/*.apk | wc -l) )) succeeded out of $(( $(cat "${allPackageListFiles[@]}" | wc -l) ))"
}

function cleanupAliasedDuplicates () {
  if ( diff -qr ./DCIM ./sdcard/DCIM &>/dev/null ); then
    rm -rvf ./sdcard/DCIM >&3
    echo "Cleaned up duplicate ./sdcard/DCIM"
  fi
  for d in ./sdcard/Android/obb/* ; do
    if ( diff -qr ./apps/"${d#./sdcard/Android/obb/}".obb "$d" &>/dev/null ); then
      rm -rvf "$d" >&3
      echo "Cleaned up duplicate $d"
    fi
  done
}

function sdcardWithExcludes () {
  if [[ $dryRun == 1 ]]; then
    echo "Would attempt to pull /storage/emulated/0 to ./sdcard via tar and pipes with excludes applied"
    return
  fi

  # Ensure cable didn't come loose
  waitForDevice "$(cat deviceId.txt)" --silent

  echo "Starting sdcard-excludes"

  echo "Excludes:" >&3
  printf '%s' "$sdcardExcludes" | sed 's/^/  /' >&3

  mkdir -pv sdcard >&3
  echo adb exec-out "printf %s $(printf %q "$sdcardExcludes") | tar -c -f - -C /storage/emulated/0 . -X -" >&3
  adb exec-out "printf %s $(printf %q "$sdcardExcludes") | tar -c -f - -C /storage/emulated/0 . -X - 2>/storage/emulated/0/tar-errors.txt" | tar -x -v -v -C sdcard -f - >&3 2>&3
  adb pull /storage/emulated/0/tar-errors.txt sdcard-errors.txt
  adb shell "rm -f /storage/emulated/0/tar-errors.txt"
  echo "Tar errors:" >&3
  cat sdcard-errors.txt >&3

  echo "Completed sdcard-excludes"
}

function rsyncWithExcludes () {
  local remoteFolder="$1"
  local localFolder="$2"

  if (! command -v rsync >&3); then
    echo "The rsync command was not found in PATH."
    exit 1
  fi
  if (! command -v adbfs >&3); then
    echo "The rsync command requires adbfs. Please install it in PATH."
    exit 1
  fi
  if (! command -v fusermount >&3); then
    echo "The rsync command requires fusermount. Please install it in PATH."
    exit 1
  fi
  if (! test -e /dev/fuse); then
    echo "Could not fine /dev/fuse. Did you remembrer to modprobe fuse?"
    exit 1
  fi

  if [[ $dryRun == 1 ]]; then
    echo "Would attempt to rsync $remoteFolder to $localFolder with excludes: $sdcardExcludes"
    return
  fi

  # Ensure cable didn't come loose
  waitForDevice "$(cat deviceId.txt)" --silent

  echo "Starting rsync $remoteFolder $localFolder"

  echo "Excludes:" >&3
  printf '%s' "$sdcardExcludes" | sed 's/^/  /' >&3

  mkdir -pv $(pwd)/adbfs-mount >&3

  echo adbfs $(pwd)/adbfs-mount >&3
  adbfs $(pwd)/adbfs-mount >&3

  # rsync very likely to return a non-zero status code
  local command_exit_code=0
  printf %s "$sdcardExcludes" | \
    rsync \
      --recursive \
      --no-inc-recursive \
      --one-file-system \
      --links \
      --times \
      --whole-file \
      --itemize-changes \
      --stats \
      --human-readable \
      --exclude-from=- \
      $(pwd)/adbfs-mount/"$remoteFolder" \
      ./"$localFolder" >&3 || command_exit_code=$?
  if [[ 0 != $command_exit_code ]]; then
    echo "rsync exit code: $command_exit_code"
  fi

  echo fusermount -u $(pwd)/adbfs-mount >&3
  fusermount -u $(pwd)/adbfs-mount >&3

  rmdir -v $(pwd)/adbfs-mount >&3
}

function sdcardExcludesClear () {
  sdcardExcludes=""
  sdcardExcludesAdd "tar-errors.txt"
}

function sdcardExcludesAdd () {
  sdcardExcludes="$sdcardExcludes$1"$'\n'
}

function sdcardExcludesReset () {
  sdcardExcludesClear
  sdcardExcludesAdd DCIM
  sdcardExcludesAdd Android/obb
}

function executeModesAndOperations () {
  while [[ $# -gt 0 ]]; do
    case "$1" in
      --unencrypted)
        abEncrypted=0
        shift 1
        ;;
      --encrypted)
        abEncrypted=1
        shift 1
        ;;
      --password-from-device-id)
        abEncrypted=1
        abPassword=""
        ;;
      --password)
        abEncrypted=1
        abPassword="$2"
        shift 2
        ;;
      --password=*)
        abEncrypted=1
        abPassword="${1#--password=}"
        shift 1
        ;;
      --appclass)
        abAppClass="$2"
        shift 2
        ;;
      --appclass=*)
        abAppClass="${1#--appclass=}"
        shift 1
        ;;
      -p|--package)
        abAppClass=package
        abPackage="$2"
        shift 2
        ;;
      -p=*)
        abAppClass=package
        abPackage="${1#-p=}"
        shift 1
        ;;
      -p=*|--package=*)
        abAppClass=package
        abPackage="${1#--package=}"
        shift 1
        ;;
      --ab-dao)
        abIncludeApk=1
        abIncludeObb=1
        shift 1
        ;;
      --ab-da)
        abIncludeApk=1
        abIncludeObb=0
        shift 1
        ;;
      --ab-d)
        abIncludeApk=0
        abIncludeObb=0
        shift 1
        ;;
      --skip-success)
        abSkipAfterFirstAppSuccess=1
        shift 1
        ;;
      --retry-success)
        abSkipAfterFirstAppSuccess=0
        shift 1
        ;;
      --ab-backupconfirm-timeout)
        abWaitingForBackupConfirmationScreenTimeout="$2"
        shift 2
        ;;
      --ab-backupconfirm-timeout=*)
        abWaitingForBackupConfirmationScreenTimeout="${1#--ab-backupconfirm-timeout=}"
        shift 1
        ;;

      ab)
        ifPaused
        stopKeepAwake
        abBackupBatch
        startKeepAwake
        shift 1
        ;;

      pull)
        ifPaused
        adbPull "$2" "$3"
        shift 3
        ;;
      dcim)
        ifPaused
        adbPull /storage/emulated/0/DCIM .
        shift 1
        ;;
      sdcard)
        ifPaused
        adbPull /storage/emulated/0 .
        shift 1
        ;;

      cleanup)
        cleanupAliasedDuplicates
        shift 1
        ;;

      --reset-excludes)
        sdcardExcludesReset
        shift 1
        ;;
      --clear-excludes)
        sdcardExcludesClear
        shift 1
        ;;
      --exclude=*)
        sdcardExcludesAdd "${1#--exclude=}"
        shift 1
        ;;
      --exclude)
        sdcardExcludesAdd "${2}"
        shift 2
        ;;

      sdcard-excludes)
        ifPaused
        sdcardWithExcludes
        sdcardExcludesReset
        shift 1
        ;;

      rsync)
        ifPaused
        rsyncWithExcludes "$2" "$3"
        sdcardExcludesReset
        shift 3
        ;;

      installers)
        ifPaused
        fetchPackageInstallers
        shift 1
        ;;
    esac
  done
}

function forceGlobalPath () {
  local commandName="$1"
  local forcePath="$2"

  if [[ "$forcePath" != "" ]]; then
    if [[ $dryRun == 1 ]]; then
      echo "Would use $forcePath for $commandName"
      return
    fi

    hash -p "$forcePath" "$commandName"
  fi
}

# Example usage: exec 1> >(prefixPipeLinesWithDate)
function prefixStdinPipedLinesWithDate () {
  perl -pe 'use POSIX strftime; $|=1; select((select(STDERR), $| = 1)[0]); print strftime "%Y-%m-%d-%H%M%S ", localtime'
}

function echoToLogInMemory () {
  if [[ $dryRun == 1 && $debug == 1 ]]; then
    echo "$@" | prefixStdinPipedLinesWithDate
  else
    printf -v inMemoryLog '%s%s\n' "$inMemoryLog" "$(echo "$@" | prefixStdinPipedLinesWithDate)"
  fi
}

function findAbeAllJar () {
  if [[ "$abeAllPath" == "" ]]; then
    local command_exit_code=0
    abeAllPath="$(command -v abe-all.jar 2>/dev/null || command_exit_code=$?)"
    if [[ "$abeAllPath" == "" ]]; then
      echo "ERROR: Unable to find the file 'abe-all.jar'. Please place it in your path or" >&2
      echo "use the --abe-all-jar option to indicate its location." >&2
      instructHowToGetHelpAndExit
    else
      abeAllPath="$(realpath "$abeAllPath")"
      echoToLogInMemory "Found abe-all.jar at $abeAllPath"
    fi
  fi
}

function waitForDevice() {
  local specificDeviceId="$1"
  local silent="$2"

  if [[ "$specificDeviceId" != "" ]]; then
    if [[ "$silent" != "--silent" ]]; then
      echo "Waiting for $specificDeviceId to be attached..."
    fi
    if [[ $dryRun == 0 ]]; then
      ANDROID_SERIAL="" adb -s "$specificDeviceId" wait-for-device
    fi
  else
    echo "Waiting for device to be attached..."
    if [[ $dryRun == 0 ]]; then
      ANDROID_SERIAL="" adb wait-for-device || echo "Continuing despite adb wait-for-device failing..." >&2
    fi
  fi

  export ANDROID_SERIAL="$(cat deviceId.txt)"
}

function adbDevices () {
  # adb devices command output format:
  # Line: "List of devices attached"
  # Device lines consisting of serial number followed by tab followed by the word "device"
  # Empty line

  if [[ $dryRun == 0 ]]; then
    ANDROID_SERIAL="" adb devices | grep '\t'
  fi
}

function verifyExpectedDeviceIdIsPresentIfSpecified () {
  local expectedDevice="$1"
  local foundDevices="$2"

  if ( ! echo -n "$foundDevices" | grep '^'"$expectedDevice"'\t' &>/dev/null ); then
    echo 'Device "'"$expectedDevice"'" not found' >&2
    instructHowToGetHelpAndExit
  fi
}

function variableLineCount () {
  local value="$1"

  # wc -l is inefficient

  # <<<"$value" adds a newline to the end (so counting then starts at 1)

  # echo -n ensures the last newline isn't sent, so if the string consists of
  # only a newline (empty output) then grep sees no line beginnings
  echo -n "$value" | grep -c '^'
}

function extractDeviceIdIfNotSpecified () {
  local forceDevice="$1"
  local foundDevices="$2"

  if [[ "$forceDevice" != "" ]]; then
    return
  fi

  # Expect there to be only one
  if [[ $( variableLineCount "$foundDevices" ) -ne 1 ]]; then
    echo "Multiple devices detected/attached to adb:" >&2
    echo "$foundDevices" >&2
    echo "Please select a device manually when calling this script." >&2
    instructHowToGetHelpAndExit
  fi

  echo -n "$foundDevices" | cut -d $'\t' -f 1 > deviceId.txt
  echoToLogInMemory "Found device id: $(cat deviceId.txt)"
}

function pair() {
  if [[ "$(cat deviceId.txt)" != "" ]] && [[ "$pairAddress" != "" ]] && [[ "$pairCode" != "" ]]; then
    ANDROID_SERIAL="" adb pair "$pairAddress" "$pairCode"
  fi
}

function connect() {
  if [[ "$(cat deviceId.txt)" != "" ]] && [[ "$pairAddress" != "" ]] && [[ "$pairCode" != "" ]]; then
    ANDROID_SERIAL="" adb connect "$(cat deviceId.txt)"
  fi
}

function disconnect() {
  if [[ "$(cat deviceId.txt)" != "" ]] && [[ "$pairAddress" != "" ]] && [[ "$pairCode" != "" ]]; then
    adb disconnect "$(cat deviceId.txt)"
  fi
}

function ensureDeviceId () {
  waitForDevice "$(cat deviceId.txt)"

  if [[ $dryRun == 1 ]]; then
    if [[ "$(cat deviceId.txt)" == "" ]]; then
      echo -n detected > deviceId.txt
    fi
    return
  fi

  local devices="$(adbDevices)"

  if [[ "$(cat deviceId.txt)" != "" ]]; then
    verifyExpectedDeviceIdIsPresentIfSpecified "$(cat deviceId.txt)" "$devices"
  else
    extractDeviceIdIfNotSpecified "$(cat deviceId.txt)" "$devices"
  fi
}

function cleanTempFiles () {
  if [[ $dryRun == 1 ]]; then
    if [[ -e .temp.tar ]]; then echo "Would remove $outputDirname/.temp.tar" ; fi
    if [[ -e .temp.tar.tf ]]; then echo "Would remove $outputDirname/.temp.tar.tf" ; fi
    if [[ -e .in-progress.ab ]]; then echo "Would remove $outputDirname/.in-progress.ab" ; fi
    return
  fi

  rm -vf \
    .temp.tar \
    .temp.tar.tf \
    .in-progress.ab
}

function copyIfNotSame () {
  local source="$1"
  local dest="$2"

  if [[ $dryRun == 1 ]]; then
    echo "Would copy $source -> $dest if not the same file"
  else
    source="$(realpath "$source")"
    dest="$(realpath "$dest")"

    if [[ "$source" != "$dest" ]]; then
      echoToLogInMemory $(cp -pv "$source" "$dest")
    fi
  fi
}

function exportPauseScript () {
  local dest="$1"

  if [[ $dryRun == 1 ]]; then
    echo "Would create $dest/pause.sh"
  else
    echo "#!/bin/bash" > "$dest/pause.sh"
    echo "touch .PAUSED" >> "$dest/pause.sh"
    chmod u+x "$dest/pause.sh"
  fi
}

function ifPaused () {
  if [[ -e .PAUSED ]]; then
    read -p "PAUSED: Press enter to resume"
    rm -f .PAUSED
  fi
}

function newTempFile() {
  local tmp="$(mktemp)"
  tempFiles=( "${tempFiles[@]}" "$tmp" )
  echo "$tmp"
}

function cleanupTempFiles() {
  if [[ ${#tempFiles} -gt 0 ]]; then
    rm "${tempFiles[@]}"
  fi
}

function addBackgrounded() {
  backgrounded=( "${backgrounded[@]}" "$@" )
}

function killAllBackgrounded() {
  if [[ ${#backgrounded} -gt 0 ]]; then
    kill "${backgrounded[@]}"
  fi
}

# Pipes and fifos:
#   1: stdout will tee to raw stdout and also be piped through prefixStdinPipedLinesWithDate and logged
#   2: stderr will tee to raw stderr and also be piped through prefixStdinPipedLinesWithDate and logged
#   3: log will be piped through prefixStdinPipedLinesWithDate and only sent to raw stdout if debug=1
#   4: raw stdout
#   5: raw stderr
# NOTE: Assumes we've already changed directory to outputDirname
function redirectOutputs () {
  local rawLogFileTemp
  local stdoutTemp
  local stderrTemp
  local debugTemp

  # Preserve raw outputs
  exec 4>&1
  exec 5>&2

  if [[ $dryRun == 1 ]]; then
    # If dry run and debugging
    if [[ $debug == 1 ]]; then
      # We won't have a real log file, so output lines to real stderr
      exec 3>&5
    fi
  else
    # Dump inMemoryLog to new log file
    echo -n "$inMemoryLog" > logs/$scriptStarted.log
    unset inMemoryLog

    # Open a background pipe for sending data to the log file which always
    # prepends the line with the date
    rawLogFileTemp="$(newTempFile)"
    tail -f "$rawLogFileTemp" >> >(prefixStdinPipedLinesWithDate >> logs/$scriptStarted.log) & addBackgrounded $!

    # If debugging, logging data will be shown on stderr, otherwise only append
    # to the log file
    if [[ $debug == 1 ]]; then
      debugTemp="$(newTempFile)"
      tail -f "$debugTemp" >> >(tee -a "$rawLogFileTemp" >&5) & addBackgrounded $!
      exec 3>> "$debugTemp"
    else
      exec 3>> "$rawLogFileTemp"
    fi

    # stdout should be logged and shown to the user
    stdoutTemp="$(newTempFile)"
    tail -f "$stdoutTemp" > >(tee -a "$rawLogFileTemp" >&4) & addBackgrounded $!
    exec 1>> "$stdoutTemp"

    # stderr should be logged and shown to the user
    stderrTemp="$(newTempFile)"
    tail -f "$stderrTemp" > >(tee -a "$rawLogFileTemp" >&5) & addBackgrounded $!
    exec 2>> "$stderrTemp"
  fi
}

function initOutputDirs () {
  if [[ $dryRun == 1 ]]; then
    if [[ ! -e "$outputDirname" ]]; then echo "Would mkdir $outputDirname" ; fi
    if [[ ! -e "$outputDirname"/logs ]]; then echo "Would mkdir $outputDirname/logs" ; fi
    if [[ ! -e "$outputDirname"/runs ]]; then echo "Would mkdir $outputDirname/runs" ; fi
    return
  fi

  mkdir -vp "$outputDirname"
  mkdir -vp "$outputDirname"/logs
  mkdir -vp "$outputDirname"/runs
}

function stayAwakeWhileCharging () {
  local stayAwake="$1" # 0 or 1

  if [[ $dryRun == 1 ]]; then
    if [[ $stayAwake == 1 ]]; then
      echo "Would instruct device to remain awake while charing"
    else
      echo "Would instruct device to allow sleep while charing"
    fi
    return
  fi

  # stay_on_while_plugged_in 0=Off 7=On
  if [[ $stayAwake == 1 ]]; then
    stayAwake=7
  fi

  # Enable Stay Awake while charging
  adb shell settings put global stay_on_while_plugged_in $stayAwake
}

function adbLogcat () {
  local outputFile="$1"

  if [[ $dryRun == 1 ]]; then
    echo "Would capture logcat output and store in $outputDirname/$outputFile"
    return
  fi

  # Capture a log before
  adb logcat -d > "$outputFile"
}

function remindUserToSetPhoneToDoNotDisturb () {
  if [[ $dryRun == 0 ]]; then
    # Remind me to set phone to Do Not Disturb before execution begins
    echo "Developer tools: Turn on USB or Wireless Debugging"
    echo "Sound: Turn on Do Not Disturb"
    echo "Recommended: Connect phone to a power source"
    read -p "Press enter to continue"
  fi
}

function cdIfNotDryRun () {
  if [[ $dryRun == 0 ]]; then
    mv deviceId.txt "$@"/deviceId.txt
    cd "$@"
  fi
}

function setDefaultOperations () {
  if [[ "${#modesAndOperations}" == 0 ]]; then
    modesAndOperations=( dcim ab sdcard installers )
  fi
}

function stripTrailingSlash () {
  echo "${1%/}"
}

function disableAdbDebugging () {
  adb shell settings put global adb_enabled 0
}

function main () {
  touch deviceId.txt
  echoToLogInMemory "Invoked as $thisScript $@"
  parseScriptArguments "$@"
  setDefaultOperations
  sdcardExcludesReset
  assignGlobalDefaults

  forceGlobalPath adb "$adbPath"
  forceGlobalPath adbfs "$adbfsPath"
  forceGlobalPath java "$javaPath"
  findAbeAllJar

  remindUserToSetPhoneToDoNotDisturb

  pair
  connect
  ensureDeviceId
  assignGlobalDefault outputDirname "$(cat deviceId.txt)-$scriptStarted"
  outputDirname="$(stripTrailingSlash "$outputDirname")"
  if [[ "${forceAppendDate}" == "true" ]]; then
    outputDirname="$outputDirname-$scriptStarted"
  fi

  initOutputDirs

  copyIfNotSame "$thisScript" "$outputDirname/$thisScriptName"
  copyIfNotSame "$abeAllPath" "$outputDirname/abe-all.jar"
  exportPauseScript "$outputDirname"

  cdIfNotDryRun "$outputDirname"

  redirectOutputs

  cleanTempFiles
  createBashScriptForThisRun "$thisScript" "$@"

  [[ $toggleStayAwake == 1 ]] && stayAwakeWhileCharging 1
  [[ $logcat == 1 ]] && adbLogcat logs/$scriptStarted.logcat.before.txt

  startKeepAwake
  executeModesAndOperations "${modesAndOperations[@]}"
  stopKeepAwake

  [[ $logcat == 1 ]] && adbLogcat logs/$scriptStarted.logcat.after.txt
  [[ $toggleStayAwake == 1 ]] && stayAwakeWhileCharging 0
  [[ $disableAdbOnComplete == 1 ]] && disableAdbDebugging
  disconnect

  echo "REMINDER: Turn off Do Not Disturb"
  echo "REMINDER: Turn off Wireless Debugging"

  killAllBackgrounded
  cleanupTempFiles
}

main "$@"
