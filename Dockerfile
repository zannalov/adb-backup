FROM alpine

RUN wget -O /usr/local/bin/abe-all.jar https://github.com/nelenkov/android-backup-extractor/releases/download/20181012025725-d750899/abe-all.jar

RUN apk update && apk upgrade

RUN \
  apk add --no-cache \
    cmd:adb \
    cmd:bash \
    cmd:perl \
    cmd:sort \
    fuse \
    libexecinfo \
    openjdk8 \
    rsync

ADD adbfs-on-alpine.patch /opt/
RUN \
  apk add --no-cache --virtual buildadbfs \
    cmd:git \
    build-base \
    fuse-dev \
    libexecinfo-dev \
    libunwind-dev \
  && cd /opt \
  && git clone https://github.com/spion/adbfs-rootless.git \
  && cd adbfs-rootless \
  && git gc --aggressive \
  && patch Makefile ../adbfs-on-alpine.patch \
  && rm ../adbfs-on-alpine.patch \
  && make \
  && apk del buildadbfs

ENV PATH="/opt/adbfs-rootless:${PATH}"

ADD adb-backup.sh /usr/local/bin/adb-backup.sh

VOLUME /workdir
WORKDIR /workdir
ENTRYPOINT ["/usr/local/bin/adb-backup.sh"]
