This is a collection of tools and commands for performing automated backups of
Android phones such that it is easier to restore individual files or
applications.

# Prerequisites
* Enable [on-device developer options](https://developer.android.com/studio/debug/dev-options) so you can use `adb`

# Usage
## Wireless and Docker
![Screenshot of Developer Options list with arrow pointing to Wireless Debugging option](./docs/Screenshot_20220220-140819.png)
* First navigate to Developer Options and enter Wireless Debugging options.

![Screenshot of Wireless Debugging detail screen highlighting toggle, IP address, and pairing option](./docs/Screenshot_20220220-140829.png)
* Turn on Wireless Debugging.
* Note the "IP address & Port" for use with `--device`.
* Tap the "Pair device with pairing code" option.

![Screenshot of Pair with Device screen highlighting IP address and pairing code](./docs/Screenshot_20220220-140836.png)
* NOTE: Leave this screen open until the backup has begun.
* Use this information for the `--pair` option (`--pair <IP Address & Port> <Wi-Fi pairing code>`, for example `--pair 192.168.1.50:23456 261700`)

### Running natively
```sh
./adb-backup.sh --device 192.168.1.50:12345 --pair 192.168.1.50:23456 654321 --password foobar --output foobar --force-append-date recommended
```

### Running via Docker
```sh
# Without fuse & adbfs:
docker run --rm -it --mount type=bind,source="$(pwd)",target=/workdir zannalov/adb-backup --device 192.168.1.50:12345 --pair 192.168.1.50:23456 261700 --password foobar --output foobar --force-append-date recommended

# With fuse & adbfs:
docker run --rm -it --device /dev/fuse --cap-add SYS_ADMIN --mount type=bind,source="$(pwd)",target=/workdir zannalov/adb-backup --device 192.168.1.50:12345 --pair 192.168.1.50:23456 261700 --password foobar --output foobar --force-append-date recommended
```

Here's a sample script for kicking off the Docker command with friendly prompts for IP address, port numbers, and pairing code:
```sh
#!/bin/sh
DEVICE_SERIAL=12345ABC6789DE
read -p "IP Address: " ip
read -p "ADB Port: " port
read -p "Pairing port: " pair
read -p "Pairing code: " code

exec docker run \
  --rm \
  -it \
  --device /dev/fuse \
  --cap-add SYS_ADMIN \
  --mount type=bind,source="$(pwd)",target=/workdir \
  zannalov/adb-backup \
    --device $ip:$port \
    --pair $ip:$pair $code \
    --password $DEVICE_SERIAL \
    --output $DEVICE_SERIAL \
    --force-append-date \
    recommended
```

#### How to Reconnect

When Docker loses the network connection and has to kill/restart the adb
server, you may need to manually reconnect.

```
docker exec -it <containerName> adb connect <ip>:<port>
```

You may also need to update the contents of the `deviceId.txt` file for the
current run if the IP or port number has changed.

## Local (USB)
* First see the "Prerequisites for running natively" below
* Connect your phone via USB cable
* Optionally run `adb devices` and explicitly pass the device id using `--device`
* Run `./adb-backup.sh`

## Options
Detailed options can be obtained via
```sh
./adb-backup.sh --help
```

# Output Directory Structure
```
$output/adb-backups.sh
$output/abe-all.jar
$output/deviceId.txt
$output/runs/<date>.sh
$output/logs/<date>.log
$output/logs/<date>.fifo
$output/logs/<date>.logcat.before.txt
$output/logs/<date>.logcat.after.txt
$output/.temp.tar
$output/.temp.tar.tf
$output/.in-progress.ab
$output/.keep-awake.txt
$output/apps/{package}.[apk|obb]
$output/backups/{package}.[no-]apk.[no-]obb.ab
```

# Prerequisites for running natively
* GNU BASH for this script
* `adb` for pulling data from Android
* `perl` for `realpath` polyfill and prefixing log lines with date and time
* `java` and [abe-all.jar](https://github.com/nelenkov/android-backup-extractor/releases/download/20181012025725-d750899/abe-all.jar) for extracting and verifying .ab files
* Optionally `fuse` and `adbfs` for `rsync` option.

# Notes
* Google Play Store package details can be found at `https://play.google.com/store/apps/details?id={{package}}`
